#include "widget.h"
#include "ui_widget.h"
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QTextCodec>
#include <QDebug>

const QString QuerySQL="SELECT * FROM `user`";

Widget::Widget(QWidget *parent):QWidget(parent),ui(new Ui::Widget)
{
    ui->setupUi(this);

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    this->setWindowTitle(tr("MySQL数据库插入和检索"));

    db=new QSqlDatabase(QSqlDatabase::addDatabase("QMYSQL"));
    db->setHostName("127.0.0.1");
    db->setDatabaseName("zeroms");
    db->setUserName("root");
    db->setPassword("");

    if(!db->open())
    {
        qDebug()<<db->lastError().text();
        return;
    }

    model=new QSqlQueryModel;
    model->setQuery(QuerySQL,*db);

    ui->tableView->setModel(model);
}

Widget::~Widget()
{
    db->close();
    delete ui;
}

void Widget::on_DoInsert_clicked()
{
    QSqlQuery query=model->query();
    query.prepare("INSERT INTO `user` (`uname`,`pwd`) VALUE (:uname,:pwd)");
    query.bindValue(":uname",ui->UNameInput->text());
    query.bindValue(":pwd",ui->PWDInput->text());
    query.exec();

    model->setQuery(QuerySQL,*db);
}
