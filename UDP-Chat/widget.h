#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QUdpSocket>

namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    QUdpSocket *conn;//连接对象
private slots:
    void onData();//收到新数据
    void onError();//发生错误
    void on_DoBind_clicked();//监听按钮
    void on_DoSend_clicked();//发送按钮
private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
