#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpServer>
#include <QMessageBox>
#include <QTextCodec>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QDesktopWidget>

namespace Ui
{
    class Widget;
}

class Widget:public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
private:
    Ui::Widget *ui;
    QTcpServer *Stcp;                               //监听
    QTcpSocket *tcp;                                //数据连接
    QString sRed;
private slots:
    void onError(QAbstractSocket::SocketError);     //当tcp出现错误时
    void newConn();                                 //当Stcp有新连接时
    void onData();                                  //当tcp收到新数据时
    void on_pBBind_clicked();                       //开始监听按钮
    void on_pBSend_clicked();                       //发送消息按钮
};

#endif // WIDGET_H
