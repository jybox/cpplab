
#include <iostream>
#include <string>
#include <vector>

using namespace std;

string toLower(string str)
{
    //讲字符串中所有的字母转化成小写
    for(string::iterator i=str.begin();i!=str.end();i++)
    {
        if(isupper(*i))
            *i=tolower(*i);
    }
    return str;
}

bool tryJava(string input,string &output)
{
    //尝试由java格式转化为CPP格式，如果出错返回false
    //处理结果在output中
    vector<string> words;//保存变量名中的每个单词

    string::iterator it=input.begin();//下面循环用的迭代器
    string::iterator lastWordBegin=input.begin();//记录这个单词开头位置的迭代器
    if( isupper(*(input.begin())))//如果第一个字母是大写，返回错误
        return false;
    while(it!=input.end())
    {//遍历变量名中的每个字符
        if(!(isalpha(*it)))//如果出现非字母，返回错误
            return false;
        if(isupper(*it) && it != input.begin())
        {//如果出现了大写字母，且这个字母不是第一个字母
            words.push_back(string(lastWordBegin,it));//把这个单词存入单词列表
            lastWordBegin=it;//修改下一个单词开头位置
        }

        it++;
    }

    words.push_back(string(lastWordBegin,it));//添加最后一个单词

    for(vector<string>::iterator i=words.begin();i!=words.end();i++)
    {//遍历单词列表中的每个单词，然后转化成cpp格式
        (*i)=toLower((*i));//将整个单词转化为小写
        if(i!=words.begin())
        {//如果这个单词不是第一个单词，输出一个下划线
            output.append("_");
        }
        output.append(*i);//输出这个单词
    }

    return true;
}

bool tryCpp(string input,string &output)
{
    //尝试由CPP格式转化为java格式，如果出错返回false
    //处理结果在output中
    vector<string> words;

    string::iterator it=input.begin();//保存变量名中的每个单词
    string::iterator lastWordBegin=input.begin();//记录这个单词开头位置的迭代器
    if( *(input.end()-1)=='_' || *(input.begin())=='_' )//如果最后一个字符或第一个字符是下划线，返回错误
        return false;

    while(it!=input.end())
    {//遍历变量名中的每个字符
        if(isupper(*it))//如果出现了大写字符，返回错误
            return false;
        if(!isalpha(*it) &&((*it)!='_'))//如果出现了字母和下划线以外的字符，返回错误
            return false;
        if(*it=='_')
        {//如果出现了下划线
            if(lastWordBegin != input.begin())//如果不是第一个字符，提取单词的话需要考虑下划线占一个字符
                words.push_back(string(lastWordBegin+1,it));
            else//如果是第一个字符，不必考虑下划线
                words.push_back(string(lastWordBegin,it));
            lastWordBegin=it;//修改下一个单词开头位置
        }
        if(*it=='_' && *(it+1) == '_')//如果连续出现了两个下划线，返回错误
            return false;

        it++;
    }

    words.push_back(string(lastWordBegin+1,it));//添加最后一个单词

    for(vector<string>::iterator i=words.begin();i!=words.end();i++)
    {//遍历单词列表中的每个单词，然后转化成java格式
        if(i!=words.begin())
        {//如果不是第一个单词，将单词首字母转化为大写
            *((*i).begin())=toupper(*((*i).begin()));
        }
        output.append(*i);//输出这个单词
    }

    return true;
}

int main()
{
    string input;
    cin>>input;
    string output;
    if(tryJava(input,output))//尝试用java方式解析，能解析就输出
        cout<<output<<endl;
    else if(tryCpp(input,output))//如果不能用java解析，就尝试用cpp解析，能解析就输出
        cout<<output<<endl;
    else//两者都不能解析，输出错误
        cout<<"Error!"<<endl;
    return 0;
}


