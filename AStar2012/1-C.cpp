
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int N=12;
    double sum=0;
    while(N--)
    {
        double i;
        cin>>i;
        sum+=i;
    }
    cout<<"$"<<setprecision(2)<<setiosflags(ios::fixed|ios::showpoint)<<(sum/12)<<endl;
    cout.flush();
    return 0;
}


