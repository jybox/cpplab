#include <iostream>
using namespace std;

int main()
{
    int n,m,min=9999;
    cin>>n>>m;
    while(m--)
    {
        int ui,di,cur=0;
        cin>>ui>>di;
        int N=n;
        while(N--)
        {
            //if(cur>=di)
            if(cur>di)
                cur-=di;
            else
                cur+=ui;
        }
        if(cur<min)
            min=cur;
    }
    cout<<min<<endl;
    return 0;
}
