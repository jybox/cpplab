#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
class QSqlDatabase;
class QSqlQueryModel;

namespace Ui
{
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
private slots:
    void on_DoInsert_clicked();
private:
    QSqlDatabase *db;
    QSqlQueryModel *model;
    Ui::Widget *ui;
};

#endif // WIDGET_H
