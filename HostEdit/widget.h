#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTableWidgetItem>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
private:
    Ui::Widget *ui;

    int rows;
    QTableWidgetItem *si;
    QString FilePath;
    QString DEFAULT_HOSTS_TEXT;

private slots:
    void Save();
    void DefHDir();
    void reLoad();
    void on_pushButton_clicked();
    void on_pBNotePad_clicked();
    void on_HList_itemClicked(QTableWidgetItem *item);
    void on_pBOK_clicked();
    void on_pBDel_clicked();
    void on_pBNew_clicked();
};

#endif // WIDGET_H
