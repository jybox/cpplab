#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :QWidget(parent),ui(new Ui::Widget)
{
    ui->setupUi(this);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    this->setWindowTitle(tr("JyIM Demo第一季-服务器程序"));
    QDesktopWidget* desktop = QApplication::desktop();
    move((desktop->width() - this->width())/2, (desktop->height() - this->height())/2);
    tcp=NULL;
    sRed=tr("<span style=\"color:red\">%1</span>");
    ui->textBrowser->append(tr("JyIM Demo第一季-服务器程序开始运行\n该程序实现了C/S结构的,一对一的,无安全验证的纯文本聊天软件。\n来自精英盒子(jybox.net) By精英王子"));
}
Widget::~Widget()
{
    delete ui;
}
void Widget::newConn()
{
    ui->pBSend->setEnabled(true);
    tcp=Stcp->nextPendingConnection();
    connect(tcp,SIGNAL(readyRead()),this,SLOT(onData()));
    connect(tcp,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(onError(QAbstractSocket::SocketError)));
    ui->textBrowser->append(sRed.arg(tr("一个客户端连接,")+tcp->peerAddress().toString()));
    Stcp->close();
}
void Widget::onError(QAbstractSocket::SocketError s)
{
    ui->textBrowser->append(sRed.arg(tr("连接错误,")+tcp->errorString()));
    ui->pBSend->setEnabled(false);
    on_pBBind_clicked();
}
void Widget::onData()
{
    QByteArray data=tcp->readAll();
    ui->textBrowser->append(sRed.arg(tr("&lt;&lt;收到消息:"))+QString(data));
}
void Widget::on_pBBind_clicked()
{
    ui->pBSend->setEnabled(false);
    ui->pBBind->setText("断开并重新监听");
    if(tcp)
    {
        tcp->abort();
        delete tcp;
        tcp=NULL;
        delete Stcp;
        Stcp=NULL;
    }
    if(Stcp)
    {
        delete Stcp;
        Stcp=NULL;
    }
    Stcp=new QTcpServer(this);
    connect(Stcp,SIGNAL(newConnection()),this,SLOT(newConn()));
    Stcp->listen(QHostAddress::Any,ui->lEPort->text().toInt());
    ui->textBrowser->append(sRed.arg(tr("监听,")+Stcp->serverAddress().toString()+tr(":")+ui->lEPort->text()));
}
void Widget::on_pBSend_clicked()
{
    QByteArray data;
    data.append(ui->lEMsg->text());
    tcp->write(data);
    ui->textBrowser->append(sRed.arg(tr("&gt;&gt;发送消息:"))+QString(data));
}
