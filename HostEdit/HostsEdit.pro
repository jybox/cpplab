#-------------------------------------------------
#
# Project created by QtCreator 2011-08-09T20:15:12
#
#-------------------------------------------------

QT       += core gui

TARGET = HostsEdit
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui

RESOURCES += \
    Res.qrc
RC_FILE = icon.rc
