#-------------------------------------------------
#
# Project created by QtCreator 2012-01-14T20:15:26
#
#-------------------------------------------------

QT       += core gui network

TARGET = UDP-Chat
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui
