#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QTextCodec>
#include <QDesktopWidget>

namespace Ui
{
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
private:
    Ui::Widget *ui;
    QTcpSocket *tcp;                                //数据连接
    QString sRed;
private slots:
    void on_pBConn_clicked();                       //连接按钮
    void onError(QAbstractSocket::SocketError);     //当tcp出现错误时
    void onConn();                                  //当连接建立成功时
    void onData();                                  //当tcp收到数据时
    void on_pBSend_clicked();                       //发送按钮
    void on_pBAbort_clicked();
};

#endif // WIDGET_H
