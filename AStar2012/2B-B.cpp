#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <iomanip>

using namespace std;

class Page
{
public:
    double x,y,z;
    int belang;
};

double minRank=999999,maxRank=0.0;

vector<Page> pageList;

int N,K;



double s(Page a,Page b)
{
    return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z);
}

void forEach(vector<Page>::iterator pos)
{
    cout<<"---"<<pos-pageList.begin()<<endl;

    for(vector<Page>::iterator it=pos+1;it!=pageList.end();it++)
    {
        cout<<setprecision(2)<<setiosflags(ios::fixed|ios::showpoint)<<(*pos).x<<" "<<(*pos).y<<" "<<(*pos).z<<" | "<<(*it).x<<" "<<(*it).y<<" "<<(*it).z;
        cout<<" "<<s(*pos,*it)<<endl;
    }
}


void calcRank(vector<Page>::iterator pos)
{
    for(vector<Page>::iterator it=pos+1;it!=pageList.end();it++)
    {
        if((*pos).belang!=(*it).belang)
        {
            double rank=s(*pos,*it);
            if(rank<minRank)
                minRank=rank;
            //else
                //cout<<rank<<endl;
            /*if(rank<0.1)
            {
                cout<<setprecision(2)<<setiosflags(ios::fixed|ios::showpoint)<<(*pos).x<<" "<<(*pos).y<<" "<<(*pos).z<<" | "<<(*it).x<<" "<<(*it).y<<" "<<(*it).z;
                cout<<" "<<s(*pos,*it)<<endl;
            }*/
        }
       /* else
        {
            cout<<"///"<<endl;
        }*/
    }
}

void makeList(vector<Page>::iterator pos)
{
    if(pos<pageList.end())
    {
        for(int i=0;i<K;i++)
        {
            (*pos).belang=i;
            makeList(pos+1);
        }
    }
    else
    {
        //cout<<"----"<<endl;
        for(vector<Page>::iterator it=pageList.begin();it!=pageList.end();it++)
        {
            //cout<<setprecision(2)<<setiosflags(ios::fixed|ios::showpoint)<<(*it).x<<" "<<(*it).y<<" "<<(*it).z<<" "<<(*it).belang<<endl;
        }

        //minRank=s(*(pageList.begin()),*(pageList.end()-1));
        minRank=999999;
        for(vector<Page>::iterator it=pageList.begin();it!=pageList.end();it++)
        {
            calcRank(it);
        }
        if(minRank>maxRank && minRank <=999998 && minRank>0.0001)
        {
            /*vector<bool> cList;
            cList.resize(K);
            for(vector<bool>::iterator it=cList.begin();it!=cList.end();it++)
            {
                *(it)=false;
            }
            for(vector<Page>::iterator it=pageList.begin();it!=pageList.end();it++)
            {
                cList[it-pageList.begin()]=true;
            }
            bool isOk=true;
            for(vector<bool>::iterator it=cList.begin();it!=cList.end();it++)
            {
                if(!*(it))
                {
                    isOk=false;
                    break;
                }
            }
            */
            int Ks=0;
            vector<int> cList;
            for(vector<Page>::iterator it=pageList.begin();it!=pageList.end();it++)
            {
                bool isIn=false;
                for(vector<int>::iterator it2=cList.begin();it2!=cList.end();it2++)
                {
                    if(*(it2)==(*it).belang)
                    {
                        isIn=true;
                        return;
                    }
                }
                if(!isIn)
                    cList.push_back((*it).belang);
            }
            if(cList.size()==K)
            {
                maxRank=minRank;
                //cout<<"----"<<endl;
                for(vector<Page>::iterator it=pageList.begin();it!=pageList.end();it++)
                {
                    //cout<<setprecision(2)<<setiosflags(ios::fixed|ios::showpoint)<<(*it).x<<" "<<(*it).y<<" "<<(*it).z<<" "<<(*it).belang<<endl;
                }
            }

            //cout<<"!!"<<endl;
        }
    }
}

int main()
{
    cin>>N>>K;
    int tN=N;
    while(tN--)
    {
        Page page;
        cin>>page.x>>page.y>>page.z;
        pageList.push_back(page);
    }
    /*for(vector<Page>::iterator it=pageList.begin();it!=pageList.end();it++)
    {
        forEach(it);
    }*/
    makeList(pageList.begin());
    cout<<setprecision(6)<<setiosflags(ios::fixed|ios::showpoint)<<maxRank<<endl;
    cout.flush();
    return 0;
}


