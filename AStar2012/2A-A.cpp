#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
#include <vector>

using namespace std;



class Item
{
public:
    int xl,xr,y;
};

bool itemCompByY ( Item elem1, Item elem2 )
{
    return elem1.y<elem2.y;
}

int main()
{
    int T;
    cin>>T;
    while(T--)
    {
        int N,M,K;
        cin>>N>>M>>K;

        vector<Item> lines;
        vector<int> ts;

        int tM=M;
        while(tM--)
        {
            Item item;
            cin>>item.xl>>item.xr>>item.y;
            lines.push_back(item);
        }

        sort(lines.begin(),lines.end(),itemCompByY);

        int myPosY=0,myPosX=K;
        ts.push_back(myPosX);
        for(vector<Item>::iterator i=lines.begin();i!=lines.end();i++)
        {
            if((*i).y>myPosY)
            {
                int me,other;
                if((*i).xl==myPosX)
                {
                    me=(*i).xl;
                    other=(*i).xr;
                }
                else if((*i).xr==myPosX)
                {
                    me=(*i).xr;
                    other=(*i).xl;
                }
                else
                {
                    continue;
                }

                myPosY=(*i).y;
                myPosX=other;
                ts.push_back(myPosY);
            }
        }

        if(myPosX==1 || myPosX==2)
        {
            cout<<"Yes"<<endl;
        }
        else
        {
            bool isOk=false;
            for(vector<int>::iterator its=ts.begin();its!=ts.end();its++)
            {
                if(isOk)
                    break;

                int myPosY=0,myPosX=K;

                for(vector<Item>::iterator i=lines.begin();i!=lines.end();i++)
                {
                    if((*i).y>myPosY)
                    {
                        int me,other;
                        if((*i).xl==myPosX)
                        {
                            me=(*i).xl;
                            other=(*i).xr;
                        }
                        else if((*i).xr==myPosX)
                        {
                            me=(*i).xr;
                            other=(*i).xl;
                        }
                        else
                        {
                            continue;
                        }

                        myPosY=(*i).y;
                        myPosX=other;

                        if((*i).y==*its)
                        {
                            if(myPosX!=N)
                            {
                                myPosX=myPosX+1;
                            }
                        }
                    }
                }

                if(myPosX==1)
                {
                    cout<<"Yes"<<endl;
                    isOk=true;
                    break;
                }

                myPosY=0;myPosX=K;

                for(vector<Item>::iterator i=lines.begin();i!=lines.end();i++)
                {
                    if((*i).y>myPosY)
                    {
                        int me,other;
                        if((*i).xl==myPosX)
                        {
                            me=(*i).xl;
                            other=(*i).xr;
                        }
                        else if((*i).xr==myPosX)
                        {
                            me=(*i).xr;
                            other=(*i).xl;
                        }
                        else
                        {
                            continue;
                        }

                        myPosY=(*i).y;
                        myPosX=other;

                        if((*i).y==*its)
                        {
                            if(myPosX!=1)
                            {
                                myPosX=myPosX-1;
                            }
                        }
                    }
                }

                if(myPosX==1)
                {
                    cout<<"Yes"<<endl;
                    isOk=true;
                    break;
                }


            }

            if(!isOk)
            {
                cout<<"No"<<endl;
            }
        }

    }
    cout.flush();
    return 0;
}
