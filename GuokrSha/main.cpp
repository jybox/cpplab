#include <QtCore>
#include <QtNetwork>

QNetworkAccessManager *manager;
QNetworkReply *reply;

QString myCookie="...";
QString myText=QString(QUrl(QString::fromUtf8("杀")).toEncoded());

QTextStream cout(stdout);

QString getHttpContent(QString url)
{
    //获得对应URL的内容为QString
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    reply=manager->get(request);

    QEventLoop waitDownload;
    QObject::connect(reply, SIGNAL(finished()), &waitDownload, SLOT(quit()));
    waitDownload.exec();

    return QString::fromUtf8( reply->readAll());
}

int getNewArtID()
{
    //返回果壳最新的0评论文章的ID
    //若没有，返回0

    QString artList=getHttpContent("http://www.guokr.com/article/");
    QRegExp rx(QString::fromUtf8("<p class=\\\"article-info\\\">评论&nbsp;(\\d+)｜推荐&nbsp;"));
    rx.indexIn(artList);

    if(rx.cap(1).toInt()==0)
    {
        QRegExp rx(QString::fromUtf8("<a target=\\\"_blank\\\" href=\\\"/article/(\\d+)/\\\">"));
        rx.indexIn(artList);
        return rx.cap(1).toInt();
    }
    else
    {
        return 0;
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    manager=new QNetworkAccessManager;

    while(true)
    {
        int id=getNewArtID();
        cout<<QDateTime::currentDateTime().toTime_t()<<"--"<<id<<endl;

        if(id)
        {
            QNetworkRequest request;
            request.setUrl(QUrl("http://www.guokr.com/api/reply/new/"));

            QByteArray content;
            QString contentStr=QString("obj_type=article&obj_id=%1&content=%2&recommend=false").arg(id).arg(myText);
            content.append(contentStr);

            QByteArray cookie;
            cookie.append(myCookie);

            request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
            request.setHeader(QNetworkRequest::ContentLengthHeader,content.length());
            request.setRawHeader("Cookie",cookie);

            reply = manager->post(request,content);

            QEventLoop waitDownload;
            QObject::connect(reply, SIGNAL(finished()), &waitDownload, SLOT(quit()));
            waitDownload.exec();

            qDebug()<<QString::fromUtf8(reply->readAll());
        }

        QEventLoop waitTimeout;
        QTimer::singleShot(20*1000, &waitTimeout, SLOT(quit()));
        waitTimeout.exec();
    }
    
    return 0;
}
