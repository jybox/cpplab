#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

enum Gift
{
    GA,GB
};

class Item
{
public:
    Gift gift;
    int x,y;
};

vector<Item> firendList;
int GAa,GAb,GAs,GBa,GBb,GBs;
int n;
int maxSum=0;

void makeList(int dep,int tGAs,int tGBs)
{
    if(dep<n)
    {
        if(tGBs)
        {
            firendList[dep].gift=GB;
            tGBs--;
            makeList(dep+1,tGAs,tGBs);
        }
        if(GAs)
        {
            firendList[dep].gift=GA;
            tGAs--;
            makeList(dep+1,tGAs,tGBs);
        }
    }
    else
    {
        int sum=0;
        for(vector<Item>::iterator it=firendList.begin();it!=firendList.end();it++)
        {
            if((*it).gift==GA)
                sum+=(*it).x*GAa+(*it).y*GAb;
            else
                sum+=(*it).x*GBa+(*it).y*GBb;
        }
        if(sum>maxSum)
            maxSum=sum;
        return;
    }
}

int main()
{
    cin>>n;
    int N=n;
    while(N--)
    {
        Item item;
        cin>>item.x>>item.y;
        firendList.push_back(item);
    }
    cin>>GAs>>GAa>>GAb>>GBs>>GBa>>GBb;
    makeList(0,GAs,GBs);
    cout<<maxSum<<endl;
    cout.flush();
    return 0;
}

