#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent):QWidget(parent),ui(new Ui::Widget)
{
    ui->setupUi(this);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    this->setWindowTitle(tr("JyIM Demo第一季-客户端程序"));
    QDesktopWidget* desktop = QApplication::desktop();
    move((desktop->width() - this->width())/2, (desktop->height() - this->height())/2);
    tcp=NULL;
    sRed=tr("<span style=\"color:red\">%1</span>");
    ui->textBrowser->append(tr("JyIM Demo第一季-客户端程序开始运行\n该程序实现了C/S结构的,一对一的,无安全验证的纯文本聊天软件。\n来自精英盒子(jybox.net) By精英王子"));

}
Widget::~Widget()
{
    delete ui;
}
void Widget::on_pBConn_clicked()
{
    if(tcp)
    {
        delete tcp;
    }
    tcp=new QTcpSocket(this);
    tcp->connectToHost(QHostAddress(ui->lEIP->text()),ui->lEPort->text().toLong());
    connect(tcp,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(onError(QAbstractSocket::SocketError)));
    connect(tcp,SIGNAL(connected()),this,SLOT(onConn()));
    connect(tcp,SIGNAL(readyRead()),this,SLOT(onData()));
    ui->pBConn->setEnabled(false);
    ui->pBAbort->setEnabled(true);
}
void Widget::onData()
{
    QByteArray data=tcp->readAll();
    ui->textBrowser->append(sRed.arg(tr("&lt;&lt;收到消息:"))+QString(data));
}
void Widget::on_pBSend_clicked()
{
    QByteArray data;
    data.append(ui->lEMsg->text());
    tcp->write(data);
    ui->textBrowser->append(sRed.arg(tr("&gt;&gt;发送消息:"))+QString(data));
}
void Widget::onError(QAbstractSocket::SocketError s)
{
    ui->textBrowser->append(sRed.arg(tr("连接错误,")+tcp->errorString()));
    ui->pBConn->setEnabled(true);
    ui->pBSend->setEnabled(false);
    ui->pBAbort->setEnabled(false);
}
void Widget::onConn()
{
    ui->textBrowser->append(sRed.arg(tr("成功连接到服务器:")+ui->lEIP->text()+tr(":")+ui->lEPort->text()));
    ui->pBSend->setEnabled(true);
}

void Widget::on_pBAbort_clicked()
{
    tcp->abort();
    delete tcp;
    tcp=NULL;
    ui->textBrowser->append(sRed.arg(tr("您手动断开了连接")));
    ui->pBConn->setEnabled(true);
    ui->pBSend->setEnabled(false);
    ui->pBAbort->setEnabled(false);
}
