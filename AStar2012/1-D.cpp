
#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    int T;
    cin>>T;
    while(T--)
    {
        int h,m;
        scanf("%d:%d",&h,&m);
        if(m!=0)
            cout<<0<<endl;
        else
        {
            h=h+12;
            if(h>24)
                h=h-24;
            cout<<h<<endl;
        }
    }
    cout.flush();
    return 0;
}

