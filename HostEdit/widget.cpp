#include "widget.h"
#include "ui_widget.h"
#include <QSettings>
#include <QMessageBox>
#include <QStringList>
#include <QString>
#include <QTextCodec>
#include <QDesktopWidget>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QAbstractItemView>
#include <QProcess>
#include <QDesktopServices>
#include <QUrl>

Widget::Widget(QWidget *parent) :QWidget(parent),ui(new Ui::Widget)
{
    try{
        ui->setupUi(this);
        //字符集
        QTextCodec::setCodecForTr(QTextCodec::codecForName("utf8"));
        //标题
        this->setWindowTitle(tr("Hosts文件编辑器-http://jybox.net 精英盒子"));
        //窗口居中
        QDesktopWidget* desktop = QApplication::desktop();
        move((desktop->width() - this->width())/2, (desktop->height() - this->height())/2);
        //绑定信号槽
        connect(ui->HDef,SIGNAL(clicked()),this,SLOT(DefHDir()));
        connect(ui->HLoad,SIGNAL(clicked()),this,SLOT(reLoad()));
        //初始化
        ui->HList->setSelectionBehavior(QAbstractItemView::SelectRows);
        ui->HList->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui->HList->setSelectionMode(QAbstractItemView::SingleSelection);
        ui->HList->horizontalHeader()->resizeSection(0,130);
        ui->HList->horizontalHeader()->resizeSection(1,170);

        DEFAULT_HOSTS_TEXT="# Copyright (c) 1993-2009 Microsoft Corp.\r\n#\r\n# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.\r\n#\r\n# This file contains the mappings of IP addresses to host names. Each\r\n# entry should be kept on an individual line. The IP address should\r\n# be placed in the first column followed by the corresponding host name.\r\n# The IP address and the host name should be separated by at least one\r\n# space.\r\n#\r\n# Additionally, comments (such as these) may be inserted on individual\r\n# lines or following the machine name denoted by a '#' symbol.\r\n#\r\n# For example:\r\n#\r\n#    102.54.94.97     rhino.acme.com          # source server\r\n#       38.25.63.10     x.acme.com              # x client host\r\n\r\n";
        si=0;
        rows=0;
        DefHDir();
        reLoad();
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在初始化的过程中出现了一个错误，而且连作者都不清楚为什么会在这时候出错"));
    }
}

void Widget::DefHDir()
{
    try{
        QString systemDir=QDir::root().path();
        ui->HDir->setText(systemDir+"windows/system32/drivers/etc/hosts");
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在加载hosts文件时位置失败，可能系统不是XP/Vista/Win7，或在Win7下没有取得管理员权限"));
    }
}
void Widget::reLoad()
{
    QString content;
    try{
        FilePath=ui->HDir->text();
        QFile file(FilePath);
        //读取文件
        if(file.open(QIODevice::ReadOnly))
        {
            char buf[2048];
            qint64 lineLen;
            while(lineLen!=-1)
            {
                lineLen=file.readLine(buf,sizeof(buf));
                content+=buf;
            }
        }
        else
        {
            QMessageBox::warning(0,tr("错误"),tr("程序在读取hosts文件时发生了一个错误，可能是您的系统中没有hosts文件(或填写的Hosts文件位置不正确)，或者在Win7下没有获得管理员权限"));
            return;
        }
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在读取hosts文件时发生了一个错误，可能是您的系统中没有hosts文件(或填写的Hosts文件位置不正确)，或者在Win7下没有获得管理员权限"));
    }
    try{
        //去除注释
        content+="\r\n";
        content.replace(tr("#"),tr("\r\n#"));
        while(1)
        {
            int a=content.indexOf(tr("#"));
            if(a==-1) break;
            int b=content.indexOf(tr("\n"),a);
            content=content.left(a)+content.mid(b+1);
        }
        //去除空格和换行
        while(1)
        {
            int a=content.indexOf(tr("\t"));
            if(a==-1) break;
            content.replace(tr("\t"),tr(" "));
        }
        while(1)
        {
            int a=content.indexOf(tr("  "));
            if(a==-1) break;
            content.replace(tr("  "),tr(" "));
        }
        while(1)
        {
            int a=content.indexOf(tr("\r\n\r\n"));
            if(a==-1) break;
            content.replace(tr("\r\n\r\n"),tr("\r\n"));
        }
        while(1)
        {
            int a=content.indexOf(tr(" \r\n"));
            if(a==-1) break;
            content.replace(tr(" \r\n"),tr("\r\n"));
        }
        while(1)
        {
            int a=content.indexOf(tr("\r\n "));
            if(a==-1) break;
            content.replace(tr("\r\n "),tr("\r\n"));
        }
        if(content.left(2)==tr("\r\n"))
        {
            content=content.mid(2);
        }
        if(content.right(2)==tr("\r\n"))
        {
            content=content.left(content.length()-2);
        }
        if(content.right(1)==tr(" "))
        {
            content=content.left(content.length()-1);
        }
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在解析hosts文件的过程中出现了一个错误，可能是您的hosts文件过于混乱，或者根本就是hosts文件语法不正确"));
    }
    try{
        //清空QTableWidget
        si=0;
        if(rows)
        {
            while(rows--)
            {
                ui->HList->removeRow(rows);
            }
        }
        //填入QTableWidget
        QStringList list=content.split(tr("\r\n"));
        rows=list.count();
        if(list.at(0)=="")
        {
            return;
        }
        for(int i=0;i<list.count();i++)
        {
            QStringList item=list.at(i).split(tr(" "));
            int row=ui->HList->rowCount();
            ui->HList->insertRow(row);
            QTableWidgetItem *i1=new QTableWidgetItem(item.at(0).trimmed());
            QTableWidgetItem *i2=new QTableWidgetItem(item.at(1).trimmed());
            ui->HList->setItem(row,0,i1);
            ui->HList->setItem(row,1,i2);
        }
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在刷新列表的过程中出现了一个错误，而且连作者都不清楚为什么会在这时候出错"));
    }
}
Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    QDesktopServices::openUrl(QUrl("http://jybox.net"));
}

void Widget::on_pBNotePad_clicked()
{
    try{
        QString qhost="notepad "+ui->HDir->text();
        QProcess::execute(qhost);
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在调用记事本的时候出现了错误，告诉你个秘密，其实你可以在开始菜单里找到记事本"));
    }
}

void Widget::on_HList_itemClicked(QTableWidgetItem *item)
{
    try{
        QString ip=ui->HList->item(item->row(),0)->text();
        QString domain=ui->HList->item(item->row(),1)->text();
        ui->lEIP->setText(ip);
        ui->lEDomain->setText(domain);
        si=item;
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在你点击列表里的项目的时候出现了一个错误，而且连作者都不清楚为什么会在这时候出错"));
    }
}
void Widget::Save()
{
    try{
        QFile file(FilePath);
        if(file.open(QIODevice::WriteOnly))
        {
            QTextStream stream(&file);
            //写文件开头的默认注释
            stream<<DEFAULT_HOSTS_TEXT<<"\n";
            for(int i=0;i<rows;i++)
            {
                stream<<ui->HList->item(i,0)->text();
                stream<<"  ";
                stream<<ui->HList->item(i,1)->text();
                stream<<"\r\n";
            }
            stream<<"\r\n#by HostEdit\r\n";
            stream<<"#Http://jybox.net\r\n";
            file.close();
        }
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在写入文件的时候出现了一个错误，可能是在Win7下没有获得管理员权限，或者文件被一些杀毒软件所保护"));
    }
    reLoad();
}
void Widget::on_pBOK_clicked()
{
    try{
        if(si)
        {
            ui->HList->item(si->row(),0)->setText(ui->lEIP->text());
            ui->HList->item(si->row(),1)->setText(ui->lEDomain->text());
        }
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在执行修改的时候出现了一个错误，而且连作者都不清楚为什么会在这时候出错"));
    }
    Save();
}

void Widget::on_pBDel_clicked()
{
    try{
        if(si)
        {
            ui->HList->removeRow(si->row());
            ui->lEIP->setText("");
            ui->lEDomain->setText("");
            si=0;
        }
        rows--;
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在执行删除的时候出现了一个错误，而且连作者都不清楚为什么会在这时候出错"));
    }
    Save();
}

void Widget::on_pBNew_clicked()
{
    try{
        int row=ui->HList->rowCount();
        ui->HList->insertRow(row);
        //如果都是空的，则填入一些默认信息
        if(ui->lEIP->text().trimmed()=="")
        {
            ui->lEIP->setText("0.0.0.0");
        }
        if(ui->lEDomain->text().trimmed()=="")
        {
            ui->lEDomain->setText("domian");
        }
        QTableWidgetItem *i1=new QTableWidgetItem(ui->lEIP->text());
        QTableWidgetItem *i2=new QTableWidgetItem(ui->lEDomain->text());
        ui->HList->setItem(row,0,i1);
        ui->HList->setItem(row,1,i2);
        rows++;
    }
    catch(...)
    {
        QMessageBox::information(0,0,tr("程序在执行新增的时候出现了一个错误，而且连作者都不清楚为什么会在这时候出错"));
    }
    Save();
}
