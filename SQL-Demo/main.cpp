#include <QtGui/QApplication>
#include <QDir>
#include "widget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qApp->addLibraryPath(QDir::toNativeSeparators(QApplication::applicationDirPath()).append(QDir::separator()).append("plugins"));

    Widget w;
    w.show();

    return a.exec();
}
