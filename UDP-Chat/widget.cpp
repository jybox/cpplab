#include <QTextCodec>
#include <QDesktopWidget>
#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent):QWidget(parent),ui(new Ui::Widget),conn(0)
{
    ui->setupUi(this);

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    this->setWindowTitle(tr("UDP消息发送Demo"));
    QDesktopWidget* desktop = QApplication::desktop();
    move((desktop->width() - this->width())/2, (desktop->height() - this->height())/2);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::onData()
{
    while(conn->hasPendingDatagrams())//如果还有数据报
    {
        QByteArray data;
        data.resize(conn->pendingDatagramSize());//将字节数组调整为和下一个数据报相通的大小
        QHostAddress sender;//等下存储发送者ip地址
        quint16 senderPort;//等下存储发送者端口

        conn->readDatagram(data.data(), data.size(),&sender, &senderPort);//读取数据

        //显示到文本框
        ui->MsgArea->append(tr("收到来自%1:%2的消息 %3").arg(sender.toString()).arg(QString::number(senderPort)).arg(QString(data)));
    }
}

void Widget::onError()
{
    ui->MsgArea->append(tr("发生错误 %1").arg(conn->errorString()));
}

void Widget::on_DoBind_clicked()
{
    if(conn)//如果连接存在，先断开连接
    {
        delete conn;
        conn=0;
    }
    conn=new QUdpSocket;//生成连接对象
    conn->bind(ui->PortInput->value());//监听端口
    connect(conn,SIGNAL(readyRead()),this,SLOT(onData()));//收到数据时，这个信号会发射
    connect(conn,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(onError()));//遇到错误时，这个信号会发射
    ui->MsgArea->append(tr("正在监听 %1").arg(ui->PortInput->value()));//显示提示消息
}

void Widget::on_DoSend_clicked()
{
    if(!conn)//如果不存在连接对象，则先创建一个
        conn=new QUdpSocket;
    ui->MsgArea->append(tr("向%1:%2发送 %3").arg(ui->IpInput->text()).arg(ui->PeerPort->value()).arg(ui->MsgInput->text()));
    QByteArray data;
    data.append(ui->MsgInput->text());
    conn->writeDatagram(data,QHostAddress(ui->IpInput->text()),ui->PeerPort->value());//法师数据报
}
