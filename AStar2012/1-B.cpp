#include <iostream>

using namespace std;

int main()
{
    int N;
    cin>>N;
    while(N--)
    {
        int K;
        cin>>K;
        int *M=new int[K];
        int sum=0;
        for(int i=0;i<K;i++)
        {
            cin>>M[i];
            sum+=M[i];
        }
        cout<<sum-K+1<<endl;
    }
    cout.flush();
    return 0;
}

