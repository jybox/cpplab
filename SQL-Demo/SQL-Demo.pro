#-------------------------------------------------
#
# Project created by QtCreator 2012-01-20T22:15:17
#
#-------------------------------------------------

QT       += core gui sql

TARGET = SQL-Demo
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui
