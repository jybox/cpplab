﻿除非特殊注明，均为GPL.  
作者：(精英王子)[http://jyprince.me/]

### AStar2012 (2012.6)
百度之星2012解题代码，仅供纪念

### AutoClicker (2012.8)
[半成品, Qt]  
鼠标自动模拟点击器  
目前仅完成了UI设计

### CS-Chat (2011.8)
[Qt]  
客户端服务器结构的简单聊天程序

### GuokrSha (2012.5)
[Qt]  
在果壳网上抢沙发的脚本，仅供纪念

### HostEdit (2011.8)
[Qt]  
hosts文件编辑器

### SQL-Demo (2012.1)
[Qt]  
QtSQL的一个小Demo

### UDP-Chat (2012.1)
[Qt]   
基于UDP的一个简易聊天程序
