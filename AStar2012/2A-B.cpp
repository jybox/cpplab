#include <iostream>
#include <string>
#include <sstream>
#include <cmath>

using namespace std;

int main()
{
    int T;
    cin>>T;
    while(T--)
    {
        unsigned long long x,a,b;
        int result=0;
        cin>>x>>a>>b;

        int bit=1;
        while(true)
        {
            if(x-pow(10,bit)<0)
                break;
            bit++;
        }
        unsigned long long X=pow(10,bit);

        for(unsigned long long i=a;i<=b;i++)
        {
            if(i%X==x)
                result++;
        }
        cout<<result<<endl;
    }
    cout.flush();
    return 0;
}

